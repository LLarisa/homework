/**
 * Задача 1.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо преобразовать первый символ переданной строки в заглавный.
 *
 * Условия:
 * - Необходимо проверить что параметр str является строкой
 */

function upperCaseFirst(str) {
    if (typeof str !== 'string') {
        throw new Error('параметр должен быть строкой');
    }
    if (str === '') {
        return str;
    };
    const result = str.charAt(0).toUpperCase() + str.slice(1);
    return result;

}

console.log(upperCaseFirst('pitter')); // Pitter
console.log(upperCaseFirst('')); // ''



