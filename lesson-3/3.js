/**
 * Задача 3.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо проверить длину строки в переменной string.
 * Если она превосходит maxLength – заменяет конец string на ... таким образом, чтобы её длина стала равна maxLength.
 * В консоль должна вывестись (при необходимости) усечённая строка.
 *
 * Условия:
 * - Переменная string должна обладать типом string;
 * - Переменная maxLength должна обладать типом number.
 */


  function truncate(string, maxlength) {
    if (typeof string != 'string') {
        throw new Error('первый параметр должен быть строкой');
    }
    if (typeof maxlength !== 'number') {
        throw new Error('второй парамент должен быть числом')
    }
    const result = string.length > maxlength ?
        string.slice(0, maxlength - 3) + '...' :
        string;
    return result;
}
console.log(truncate('Вот, что мне хотелось бы сказать на эту тему:', 21)); 


