/**
 * Задача 2.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо присвоить переменной result значение true, 
 * если строка source содержит подстроку spam. Иначе — false.
 *
 * Условия:
 * - Необходимо выполнить проверку что source и spam являются типом string.
 * - Строки должны быть не чувствительными к регистру
 */


function checkSpam(source, spam) {
    if (typeof source !== 'string') {
        throw new Error('первый парамент должен быть строкой')
    }
    if (typeof spam !== 'string') {
        throw new Error('второй парамент должен быть строкой')
    }
    const lowerCasedSource = source.toLowerCase();
    const lowerCasedSpam = spam.toLowerCase();
    const result = lowerCasedSource.includes(lowerCasedSpam)
    return result;
}

console.log(checkSpam('pitterXXX@gmail.com', 'xxx'));
console.log(checkSpam('pitterxxx@gmail.com', 'XXX'));
console.log(checkSpam('pitterxxx@gmail.com', 'sss'));
  
